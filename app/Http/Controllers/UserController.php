<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;


/**
 * Class DepositController
 */
class UserController extends Controller
{
    public $title = 'User';
    protected $link = 'user';

    /**
     * Display listing page.
     */
    public function index()
    {
        $page = collect();
        $page->title = $this->title;
        $page->link = url($this->link);
        return view('list', compact('page'));
    }

    /**
     * Display a listing of the resource in datatable.
     * @throws \Exception
     */
    public function lists(Request $request)
    {
        $detail = User::select(['name', 'email', 'phone', 'id']);
        $detail->latest();
        return DataTables::of($detail)
            ->addColumn('action', function ($detail) {
                $action = '';
                $action .= '<a class="btn btn-primary btn-sm mr-2" href="javascript:void(0)" onclick="ajaxManage(' . $detail->id . ')"><i class="nav-link-icon fa fa-pencil"></i><span> Edit </span></a>';
                $action .= '<a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="ajaxDelete(' . $detail->id . ')"><i class="nav-link-icon fa fa-times-circle"></i><span> Delete </span></a>';
                return $action;
            })
            ->removeColumn(['id'])
            ->escapeColumns([])
            ->make(false);
    }

    /**
     * Show the form for create the specified resource.
     */
    public function create()
    {
        $page = collect();
        $page->title = $this->title;
        $page->link = url($this->link);
        $page->form_url = url($this->link);
        $page->form_method = 'POST';

        $user = [];
        return view('manage', compact('page', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     * @param UserRequest $request
     * @return array
     */
    public function store(UserRequest $request)
    {
        $user = New User();
        $this->saveDB($user, $request);

        $url = url($this->link);
        $error = false;
        $message = Str::singular(Str::title($this->title)) . ' added successfully';
        return compact('error', 'message', 'url');
    }

    /**
     * @param $table
     * @param $request
     */
    public function saveDB($table, $request)
    {
        $table->name = $request->name;
        $table->email = $request->email;
        $table->phone = $request->phone;
        $table->save();
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $page = collect();
        $page->title = $this->title;
        $page->link = url($this->link);
        $page->form_url = url($this->link . '/' . $user->id);
        $page->form_method = 'PUT';
        return view('manage', compact('page', 'user'));
    }

    /**
     * Update the specified resource in storage.
     * @param UserRequest $request
     * @param $id
     * @return array
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $this->saveDB($user, $request);

        $url = url($this->link);
        $error = false;
        $message = Str::singular(Str::title($this->title)) . ' saved successfully';
        return compact('error', 'message', 'url');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user) {
            User::destroy($id);
            $error = false;
            $message = 'User Deleted successfully';
        } else {
            $error = true;
            $message = 'User could not be deleted';
        }
        return compact('error', 'message');
    }
}
