@extends('layouts.app')
@section('title', Str::title($page->title))
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <a onclick="ajaxManage()" href="javascript:void(0)"
                   class="btn btn-success btn-sm pull-right">
                    <i class="fa fa-list-alt"></i> Add {{ Str::title($page->title) }}
                </a>
            </div>

            <div class="col-sm-12 data-table-container">
                <table class="table table-hover table-striped table-bordered data-tables"
                       data-url="{{ url($page->link.'/lists') }}" data-form="page"
                       data-length="20">
                    <thead>
                    <tr>
                        <th data-orderable="false">Full Name</th>
                        <th data-orderable="false">Email</th>
                        <th data-orderable="false">Phone</th>
                        <th data-orderable="false">Options</th>
                    </tr>
                    </thead>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('page-modals')
    <div class="modal fade" id="manageModal" tabindex="-1" role="dialog" aria-labelledby="manageModal"
         aria-hidden="true">
    </div>
@endpush
@push('page-scripts')
    <script>
        var link = '{{ url($page->link) }}';
    </script>
@endpush
