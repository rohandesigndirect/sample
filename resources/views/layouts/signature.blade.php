<div class="modal fade" id="signatureModal" tabindex="-1" role="dialog" aria-labelledby="signatureModal"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <div id="type" class="d-inline"></div>
                    Signature
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="container"></div>
            </div>
            <div class="modal-footer">
                {!! Form::hidden('type', '', ['id' => 'type_text']) !!}
                <button type="button" id="signature_clear" class="btn btn-secondary">Clear</button>
                <button type="button" id="signature_submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/konva@4.0.18/konva.min.js"></script>
<script type="text/javascript" src="{{ asset('js/pages/signature.js') }}"></script>
