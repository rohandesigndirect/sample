@php
    use App\Models\Company;
    use App\Models\House\House;use App\Models\Notification;
    use Illuminate\Support\Facades\Auth;
    $enquires=Notification::where('seen',1)->where('type_id',1)->get();
    $users=Notification::where('seen',1)->where('type_id',2)->get();
    $orders=Notification::where('seen',1)->where('type_id',3)->get();
    $stocks=Notification::where('seen',1)->where('type_id',4)->get();
    $subscribers=Notification::where('seen',1)->where('type_id',5)->get();
    $user=Auth::user();
    if(Session::get('company_id')!=''){
        $company=Company::find(Session::get('company_id'),['id','name']);
    }
    if(Session::get('house_id')!=''){
        $selected_house=House::find(Session::get('house_id'),['id','name']);
    }
@endphp
<div class="app-header header-shadow">
    <div class="app-header__logo">
        <div class="logo-src"><img src="{{ asset('images/logo.png') }}" style="height: 38px;"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                        data-class="closed-sidebar">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
            <span>
                <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                    <span class="btn-icon-wrapper">
                        <i class="fa fa-ellipsis-v fa-w-6"></i>
                    </span>
                </button>
            </span>
    </div>
    <div class="app-header__content">
        @if (Session::get('company_id')!='' && $user->hasPermission('companies'))
            <div class="widget-content p-0">
                <div class="widget-content-wrapper">
                    <div class="widget-content-left">
                        <div class="btn-group">
                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                               class="p-0 btn">
                                <i class="fa fa-3x fa-building ml-2 opacity-8"></i>
                            </a>
                        </div>
                    </div>
                    <div class="widget-content-left  ml-3 header-user-info">
                        <div class="widget-heading">
                            {{ $company->name }}
                        </div>
                        <div class="widget-subheading">
                            {{ $user->roles[0]->name }}
                        </div>
                    </div>
                    <div class="widget-content-right header-user-info ml-3">
                        <a href="{{ url('companies/logout') }}" data-toggle="tooltip" data-placement="bottom"
                           class="btn-shadow p-1 btn btn-sm" title="Company Logout">
                            <i class="fa fa-2x fa-sign-out-alt pr-1 pl-1"></i>
                        </a>
                    </div>
                </div>
            </div>
        @endif
        @if (Session::get('house_id')!='')
            <div class="widget-content p-0">
                <div class="widget-content-wrapper">
                    <div class="widget-content-left">
                        <div class="btn-group">
                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                               class="p-0 btn">
                                <i class="fa fa-3x fa-building ml-2 opacity-8"></i>
                            </a>
                        </div>
                    </div>
                    <div class="widget-content-left  ml-3 header-user-info">
                        <div class="widget-heading">
                            {{ $selected_house->name }}
                        </div>
                        <div class="widget-subheading">
                            {{ $user->roles[0]->name }}
                        </div>
                    </div>
                    @if($user->roles[0]->id==1 || $user->roles[0]->id==2 || $user->roles[0]->id==3)
                        <div class="widget-content-right header-user-info ml-3">
                            <a href="{{ url('house/logout') }}" data-toggle="tooltip" data-placement="bottom"
                               class="btn-shadow p-1 btn btn-sm" title="House Logout">
                                <i class="fa fa-2x fa-sign-out-alt pr-1 pl-1"></i>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        @endif
        <div class="app-header-right">

            <div class="header-btn-lg pr-0">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="btn-group">
                                <img src="{{ $user->staff->show_image ?? asset('images/default-user.png') }}"
                                     class="img-thumbnail"
                                     style="height:50px;width:50px">
                            </div>
                        </div>
                        <div class="widget-content-left  ml-3 header-user-info">
                            <div class="widget-heading">
                                {{ $user->name }}
                            </div>
                            <div class="widget-subheading">
                                {{ $user->roles[0]->name }}
                            </div>
                        </div>
                        <div class="widget-content-right header-user-info ml-3">
                            <a href="{{ url('password/change') }}" data-toggle="tooltip" data-placement="bottom"
                               class="btn-shadow p-1 btn btn-sm" title="Change Password">
                                <i class="fa fa-2x fa-key pr-1 pl-1"></i>
                            </a>
                        </div>
                        <div class="widget-content-right header-user-info ml-3">
                            <a href="{{ url('logout') }}" data-toggle="tooltip" data-placement="bottom"
                               class="btn-shadow p-1 btn btn-sm" title="Logout">
                                <i class="fa fa-2x fa-sign-out-alt pr-1 pl-1"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
