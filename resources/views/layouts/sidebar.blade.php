<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src">Midco</div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                        data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
                    <span>
                        <button type="button"
                                class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                            </span>
                        </button>
                    </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Menu</li>
                <li {!! (Request::is('/') ? 'class="mm-active"' : '') !!}>
                    <a href="{{ url('/') }}">
                        <i class="metismenu-icon fa fa-tachometer-alt"></i>
                        Dashboard
                    </a>
                </li>
                @if (Auth::user()->hasPermission('companies'))
                    <li {!! (Request::is('companies*') ? 'class="mm-active"' : '') !!}>
                        <a href="{{ url('companies') }}">
                            <i class="metismenu-icon fa fa-building"></i>
                            Companies
                        </a>
                    </li>
                @endif
                @if(Session::get('company_id')!='')
                    @if (Auth::user()->hasPermission('houses'))
                        <li {!! Request::is('houses*') ? 'class="mm-active"' : '' !!}>
                            <a href="{{ url('houses') }}">
                                <i class="metismenu-icon fa fa-home"></i>
                                Homes
                            </a>
                        </li>
                    @endif
                    @if (Auth::user()->hasPermission('roles'))
                        <li {!! Request::is('roles*') ? 'class="mm-active"' : '' !!}>
                            <a href="{{ url('roles') }}">
                                <i class="metismenu-icon fa fa-user-cog"></i>
                                Staff Role
                            </a>
                        </li>
                    @endif
                    @if (Auth::user()->hasPermission('staff'))
                        <li {!! Request::is('staff*') ? 'class="mm-active"' : '' !!}>
                            <a href="{{ url('staff') }}">
                                <i class="metismenu-icon fa fa-user-tie"></i>
                                Staff
                            </a>
                        </li>
                    @endif
                    @if (Auth::user()->hasPermission('service_users') || Auth::user()->hasPermission('view_service_users'))
                        <li {!! (Request::is('service-users*')||Request::is('forms*') ? 'class="mm-active"' : '') !!}>
                            <a href="{{ url('service-users') }}">
                                <i class="metismenu-icon fa fa-users"></i>
                                Tenants / Residents
                            </a>
                        </li>
                    @endif
                @endif
                <li {!! (Request::is('reminders*') ? 'class="mm-active"' : '') !!}>
                    <a href="{{ url('reminders/calender') }}">
                        <i class="metismenu-icon fa fa-calendar-alt"></i>
                        Reminders
                    </a>
                </li>
                <li>
                    <a href="http://www.rolandhomes.co.uk/" target="_blank">
                        <i class="metismenu-icon fa fa-globe"></i>
                        Website
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
