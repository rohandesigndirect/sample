<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{ env('APP_NAME') }} - @yield('title')</title>
    <base href="{{ url('/') }}"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"
    />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('layouts.general_css')
</head>
<body>

<div class="container">
    <div class="content">
        <div class="title mt-4">
            {{ $page->title }} Lists
        </div>
        @yield('content')
    </div>
    @stack('page-modals')
    <script>
        var basePath = '{{ url('/') }}';
    </script>
    @include('layouts.general_js')
    @stack('page-scripts')
</div>
</body>
</html>

