<div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
                <div id="type" class="d-inline"></div>
                {{ $user?'Edit':'Add' }} User
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        {!! Form::open(['url' => $page->form_url, 'method' => $page->form_method,'id'=>'user-form','class'=>'ajax-submit']) !!}
        <div class="modal-body">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        {!! Form::label('name', 'Full Name*', ['class' => 'control-label']) !!}
                        {!! Form::text('name', $user->name ?? '', ['class' => 'form-control','placeholder'=>'Enter Full Name*']) !!}
                        <label id="name-error" class="error invalid-feedback"></label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        {!! Form::label('email', 'Email*', ['class' => 'control-label']) !!}
                        {!! Form::email('email', $user->email ?? '', ['class' => 'form-control','placeholder'=>'Enter Email*']) !!}
                        <label id="email-error" class="error invalid-feedback"></label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        {!! Form::label('phone', 'Phone*', ['class' => 'control-label']) !!}
                        {!! Form::text('phone', $user->phone ?? '', ['class' => 'form-control','placeholder'=>'Enter Phone*']) !!}
                        <label id="phone-error" class="error invalid-feedback"></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>

