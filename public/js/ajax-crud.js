var form;
var scrollTop = 2000;

$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.data-table-container').each(function () {
        var formValue;
        var columns;
        var length;
        var form;
        var url;
        var table;
        table = $(this).find('.data-tables');
        url = table.data('url');
        form = table.data('form');
        length = table.data('length');
        columns = [];
        formValue = [];
        table.find('thead th').each(function () {
            var column = {'data': $(this).data('column')};
            columns.push(column);
        });

        table.DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            bLengthChange: false,
            searching: false,
            pageLength: length,
            ajax: {
                "type": "GET",
                "url": url,
                "data": function (data) {
                    data.form = formValue;
                }
            },
            columns: columns,
        });
    });
});

$("body").on("submit", ".ajax-submit", function (e) {
    var field;
    e.preventDefault();
    field = this.id;
    form = $('#' + field);
    $('.error').hide();
    var formData = new FormData($(this)[0]);
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: formData,
        processData: false,
        contentType: false,
        success: dataRefresh,
        error: ajaxError
    });
});

function ajaxError(html) {
    if (html.status === 422) {
        showAlert("error", "Please enter required fields");
        var errors = html.responseJSON.errors;
        var i = 1;
        $.each(errors, function (key, value) {
            var id = "#" + key + "-error";
            $(id).html(value);
            $(id).show();
        });
        $(".form-control").keyup(function () {
            $(this).closest(".error").hide();
        });
    } else if (html.status === 403) {
        showAlert("error", "User not authorised");
    } else {
        showAlert("error");
    }
}

function ajaxManage(id = null) {
    var url = id == null ? link + "/create" : link + "/" + id + "/edit";
    $.ajax({
        url: url,
        type: "GET",
        dataType: "html",
    })
        .done(function (html) {
            $('#manageModal').html(html);
            $('#manageModal').modal({backdrop: 'static', keyboard: false})
            // $('#manageModal').modal('show');
        })
        .fail(function () {
            showError();
        });
}

function ajaxDelete(id) {
    Swal.fire({
        title: 'Are you sure?',
        showCancelButton: true,
        confirmButtonText: `Delete`,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: link + "/" + id,
                type: "DELETE",
                dataType: "html",
                success: dataRefresh,
                error: showError
            });
        }
    });
}

function dataRefresh(data) {
    if (data.error === true) {
        showAlert("error", data.message);
    } else {
        $('.data-tables').DataTable().ajax.reload(null, false);
        $('#manageModal').modal('hide');
        showAlert('success', data.message);
    }
}

function showError() {
    var failMsg = 'Some thing went wrong, Please try Again';
    showAlert("error", failMsg);
}

function showAlert(type, msg = null) {
    var message;
    if (type == 'error') {
        message = msg == null ? 'Some thing went wrong, Please try Again' : msg;
    } else {
        message = msg == null ? 'Success' : msg;
    }
    Swal.fire(message, 'Ok, got it!', type);
}
